# Simple Demonstration HTML/JS UI

This is a demonstration User Interface for the "Microservices Investments Management Platform" project.

This UI is designed to serve two main purposes:
1. To demonstrate the fundamentals of sending HTTP GET/POST requests from javascript.
2. To demonstrate the MOST MINIMAL functionality of the investments management platform.


### Features
The UI is in a single file for simplicity of demonstration. You should separate the Javascript and HTML into separate files if using this UI.

This UI interfaces to two main systems:

1. A Trade API that will allow the user to request to make a Trade. (allowing users to make an investment)
2. A Live Price service that will allow the UI to read live Stock prices. The Live Price service is implemented as an AWS Lambda.

### Price Service
The Live Price Service Lambda is stored on the "conygretraining" AWS Account. It is in the Ireland Region and called "simplePriceFeed".

The link below will bring you to that Lambda IF you are logged in to the "conygretraining" AWS account.

<https://eu-west-1.console.aws.amazon.com/lambda/home?region=eu-west-1#/functions/simplePriceFeed?tab=configuration>


### CORS
In order for a UI to interface to a backend service that is at a different host from the UI the backend must allow "Cross Origin Requests".

#### CORS in Spring-Boot Java
To allow Cross Origin Requests from any source in your spring-boot microservice you should add the following annotation to the your REST Controller class(es):
```@CrossOrigin(origins="*") ```

The corresponding import is:
```import org.springframework.web.bind.annotation.CrossOrigin;```

An example of what the top of a REST Controller Class would look like with this annotation added is:
```
@CrossOrigin(origins="*") 
@RestController
@RequestMapping("/v1/trade")
public class TradeController {

    @Autowired
    private TradeService tradeService;
```

#### CORS in AWS Lambda
To allow Cross Origin Requests from any source in a Python AWS Lambda you should include the following "headers" section in the dictionary returned by your lambda:
```
'headers': {
                "Access-Control-Allow-Headers": "Content-Type",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
            },
```

A full return statement that includes these headers would look like (the 'body' section will be specific to your lambda!):
```
return {'statusCode': 200,
            'headers': {
                "Access-Control-Allow-Headers": "Content-Type",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
            },
            'body': json.dumps({'ticker': ticker,
                                'price_data': price_data})}
```
